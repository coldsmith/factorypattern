package main.java;

public class ConcreteFactory1 implements AbstractFactory{
	public AbstractProductA createProductA(){
		return new ProductA1("ProductA1");
	}
	public AbstractProductB createProductB(){
		return new ProductB1("ProductB1");
	}
}

