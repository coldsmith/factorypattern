package main.java;

public interface AbstractFactory {
	abstract AbstractProductA createProductA();
	abstract AbstractProductB createProductB();
}

