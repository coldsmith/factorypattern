package main.java;

public class ConcreteFactory2 implements AbstractFactory{
	public AbstractProductA createProductA(){
		return new ProductA2("ProductA2");
	}
	public AbstractProductB createProductB(){
		return new ProductB2("ProductB2");
	}
}
